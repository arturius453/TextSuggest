#include <iostream>
#include <string>
#include <sstream>

#include <cstdlib>

#include "../../lib/subprocess2.hpp"

namespace sp2 = subprocess2;

int main(int argc, char ** argv) {
	
	std::vector<std::string> args(argv, argv+argc);

	if (argc < 3) {
		return 3;
	}

	std::string op = args[1];
	std::string text = args[2];

	if (op == "matches") {
		if (text.substr(0, 1) == "$") {
			return 0;
		} else {
			return 1;
		}
	} else if (op == "process") {
		text.erase(0, 1);
		try {
			auto p = sp2::check_output({text}, sp2::shell{true});
			std::cout << p.buf.data();
		}
		catch (std::exception) {
			// if command fails do nothing
		}

	}
	
	return 0;

}